#!/bin/bash

# create symlinks for config files that need to exist in home
#
# inputs
# 1: prefix: path to where all of the symlinks will be created (default: ~/)

#
# check input args
#
if [[ "$#" -gt 1 ]]; then
  echo "Usage: setup.sh <Prefix>"
  echo "Prefix: path where all of the symlinks will be created (default: ~/)"
  exit -1
fi
prefix="~/"
if [[ -d $1 ]]; then
  prefix=$1
fi

#
# check if the destination file already exists, 
# if it does print an error message, 
# otherwise create the symlink
#
# @param $1: target of the symlink
# @param $2: destination of the symlink
# @param $3: -d for directory, nothing for file
#
createLink()
{
  #check if the destination already exists (be it a file or directory)
  if [[ $3 == "-d" ]]; then
    #linking directory
    if [[ -d $2 ]]; then
      echo "$2 already exists, skipping"
    else
      echo "linking: $1-->$2"
      ln -sd $1 $2
    fi
  else
    #linking file
    if [[ -f $2 ]]; then
      echo "$2 already exists, skipping"
    else
      echo "linking: $1-->$2"
      ln -s $1 $2
    fi
  fi
}

#
# bash files
#
createLink $(realpath ./bash/.bash_profile) $prefix/.bash_profile
createLink $(realpath ./bash/.bashrc) $prefix/.bashrc
createLink $(realpath ./bash/.inputrc) $prefix/.inputrc

#
# tmux
#
createLink $(realpath ./tmux/.tmux.conf) $prefix/.tmux.conf

#
# vim
#
createLink $(realpath ./vim/.vim) $prefix/.vim -d
createLink $(realpath ./vim/.vimrc) $prefix/.vimrc

#
# xorg
#
createLink $(realpath ./xorg/.xinitrc) $prefix/.xinitrc
createLink $(realpath ./xorg/.Xresources) $prefix/.Xresources

#
# git
#
createLink $(realpath ./git/.gitconfig) $prefix/.gitconfig
