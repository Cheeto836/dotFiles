"disable modelines (security)
set nomodeline

"set leader character
let mapleader="."

"""""""""""""""""""""""""""""""""""
"PLUGINS
"""""""""""""""""""""""""""""""""""
"pathogen setup since its a submodule
runtime bundle/vim-pathogen/autoload/pathogen.vim

" temp disable rainbow parens
let g:pathogen_disabled = []
call add(g:pathogen_disabled, 'vim-rainbow')

"Load plugins with pathogen
execute pathogen#infect()

"configure vim-cpp-enhanced-highlight
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
"more accurate template highlighting, but slow for big files
let g:cpp_experimental_simple_template_highlight = 1
let g:cpp_concepts_highlight = 1

let g:loaded_matchparen=1

"configure CoC
set completeopt-=preview
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice
"inoremap <silent><expr> <CR> coc#pum#visible() && coc#pum#info()['index'] == -1
  "\ ? "\<C-e>\<CR>"
  "\ : coc#pum#visible()"\<CR>"
    "\ ? coc#_select_confirm()
    "\ : "\<CR>"


function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

nnoremap <leader>cf <Plug>(coc-format)
"bring up fix it menu
nnoremap <leader>fi <Plug>(coc-fix-current)
nnoremap <leader>gd <Plug>(coc-definition)

" Use K to show documentation in preview window
nnoremap <silent> <leader>d :call ShowDocumentation()<CR>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

"configure NerdCOMMENTER
let g:NERDCompactSexyComs = 1

"configure vim-rainbow
let g:rainbow_active = 1
let g:rainbow_guifgs = [
      \ '#A599E9',
      \ '#9EFFFF',
      \ '#FF7200',
      \ '#3AD900',
      \ '#EDCE07'
      \ ]

"""""""""""""""""""""""""""""""""""
"COLORS
"""""""""""""""""""""""""""""""""""
"shades_of_purple
set t_8f=[38;2;%lu;%lu;%lum
set t_8b=[48;2;%lu;%lu;%lum
set t_ut=
set t_Co=256
set termguicolors
colorscheme silverhand
let g:airline_theme='silverhand'

"space-vim-dark
"set t_co=256
"colorscheme space-vim-dark
"set background=dark

"""""""""""""""""""""""""""""""""""
"GENERAL SETTINGS
"""""""""""""""""""""""""""""""""""
"make vim more useable
set nocompatible "disable vi compatibilities
filetype plugin indent on "filetype detection, filetype specific plugins, filetype indent
syntax on "syntax highlighting
set splitbelow splitright "split to the right and below instead of left and above

"encodeing
set encoding=utf-8

"De-clutter
set nobackup
set noswapfile

"Line numbers
set number
set numberwidth=4

"Tabs, indents, etc
set tabstop=2
set shiftwidth=2
set expandtab
set autoindent
set smarttab

"search
set hlsearch
set incsearch

"no word wrap
set nowrap

"Code folding
set foldenable
set foldmethod=indent
set foldlevel=99

"showing filename when only one file is open
set laststatus=2
set statusline="%f"

"""""""""""""""""""""""""""""""""""
"AUTO COMMANDS
"""""""""""""""""""""""""""""""""""
"set filetypes that vim doesn't automatically detect
autocmd! BufNewFile,BufRead *.ino setlocal ft=arduino "arduino files
autocmd! BufNewFIle,BufRead *.cprj setlocal ft=vim "custom made project files are actualy just vimscript
autocmd! BufNewFile,BufRead *qcprj setlocal ft=xml "treat qcproj (qcp project, RoboBees Qucik Change Parameter files) files like xml
autocmd! BufNewFile,BufRead *qcprf setlocal ft=xml "treat qcproj (qcp project, RoboBees Quick Change Parameter files) files like xml
autocmd! BufNewFile,BufRead *qcprg setlocal ft=xml "treat qcproj (qcp project, RoboBees Quick Change Parameter files) files like xml

"disable auto insertion of comments in all files
autocmd! FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"automatically delete trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

"adding files to vim buffers based on open command used for vim
function AddNewFiles()
  for file in systemlist('$XDG_CONFIG_HOME/bash/scripts/vimBufferList.sh')
    execute "badd " . file
  endfor
endfunction
nnoremap <leader>af :call AddNewFiles()<CR>

"session handling
nnoremap <leader>os :source ~/.cache/personal/vim_sessions/
nnoremap <leader>ss :mks! ~/.cache/personal/vim_sessions/

"fix folding since sometimes it doesn't work quite right
nnoremap <leader>ff :set foldmethod=indent<CR>

"print current line number at the cursor
nnoremap <leader>ln :execute "normal! i" . ( line("."))<cr>

