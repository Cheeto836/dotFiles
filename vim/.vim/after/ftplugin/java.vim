so ~/.vim/after/languageAddons/braketed.vim
setlocal cindent
nnoremap <buffer> ; A;<esc>
nnoremap <buffer> <leader>jd O/**<CR><backspace><space>*<CR>*/<esc>kA<space>

"4 space tabs for java
set tabstop=4
set shiftwidth=4
