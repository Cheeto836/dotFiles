"add brackets after the current line
nnoremap <buffer> t{ A<backspace><esc>o{<enter>}<esc>

"default end (loop/if/case/...) comment (make functions at some point)
nnoremap ec A<space>//end<space><esc>p
nnoremap dec I<esc>llld$x

"copy current line without new line
nnoremap cl I<esc>v$hy<esc>

