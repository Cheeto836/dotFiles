#===================================================
#
# ~/.bashrc
#
#===================================================

#===================================================
#
# environment setup
#
#===================================================
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#start tmux if in a graphical terminal
#[[ ! -z "$DISPLAY" ]] && [[ -z "$TMUX" ]] && exec tmux

#git auto-completion
source ~/.config/git/git-completion.bash

#===================================================
#
# aliases
#
#===================================================
#ls aliases
alias ls='ls --color=auto --group-directories-first'
alias la='ls -a --color=auto --group-directories-first'
alias ll='ls -l -h --color=auto --group-directories-first'
alias lla='ls -l -a -h --color=auto --group-directories-first'

alias tree='tree --dirsfirst'

alias vi="vim"

alias gitl='git log --decorate --oneline --graph --branches --all --date=short --format="%C(auto)%h%d %s %C(cyan)%ad %C(magenta bold)%an"'
alias gitsu='git submodule update --init --recursive'

alias godot='devour.sh Godot_v3.2.2-stable_x11.64'

#quick path jump aliases
alias odyssey='devour.sh zathura /data/dnd/dm/odyssey_of_the_dragonlords/campaign_docs/Odyssey_of_the_Dragonlords_v1.pdf'
alias tiadesias='cd /data/dnd/dm/tiadesias'

#===================================================
#
# exports
#
#===================================================
#setup XDG base directories
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DOCUMENTS_DIR=$HOME/documents

#add things to path variable
export PATH="$XDG_CONFIG_HOME/bash/scripts:"$PATH
export PATH="$XDG_CONFIG_HOME/bash/scripts/devour:"$PATH
export PATH="$HOME/tools:"$PATH
#export PATH="$HOME/Qt/5.14.2/gcc_64/bin:"$PATH #TODO make this a sed command to automatically acomadate new versions of Qt that get installed
#export PATH="$HOME/Qt/Tools/QtCreator/bin:"$PATH
export PATH="$XDG_CONFIG_HOME/bash/scripts/PromptScripts:"$PATH
export PATH=$(echo $PATH | tr ':' '\n' | sort -u | tr '\n' ':' | sed 's,:$,\n,')

export XZ_DEFAULTS="-T 8"

#export PS1="\e[0;32m[\u@\h]\e[m \e[1;33m\w\e[m$ "
#export PS1="\e[0;32m[\u@\h]\e[m \e[1;33m$(pwd | sed 's-/home/cheeeto-~-g')\e[m \e[0;33m\$(. DirStack.sh)\e[m\e[0;36m\$(Branch.sh)\e[m\n\$ "
export PS1="\e[0;32m[\u@\h]\e[m \e[1;33m\w\e[m \e[0;33m\$(. DirStack.sh)\e[m\e[0;36m\$(Branch.sh)\e[m\n\$ "

#make cmake work with QT
#export CMAKE_PREFIX_PATH="$HOME/Qt/5.14.2/gcc_64/bin"

#Make android-studio window start instead of be a blank window
export _JAVA_AWT_WM_NONREPARENTING=1

#set C/C++ compilers
#export CC=/usr/bin/clang
#export CXX=/usr/bin/clang++

#personal use environment variables
export VIM_SESSION_FILES=$XDG_CACHE_HOME/personal/vim_sessions

#ignore duplicate bash commands in history
export HISTCONTROL=ignoredups

#setup default programs
export EDITOR=vim
export BROWSER=brave

#===================================================
#
# Robotics specific
#
#===================================================
#get current year for figuring out which WPI directory to use
export WPI_YEAR=$(date +"%Y")

#build/tests aliases for gradle
frc ()
{
  if [[ $# -lt 1 ]]; then
    echo "must provide an action. examples of valid actions are: build, test, javadoc"
    return -1
  fi

  if [[ ! -f ./gradlew ]]; then
    #TODO make eventually use a find command
    echo "no gradlew file found in current directory"
    return -1
  fi

  command=$1
  shift
  ./gradlew $command -Dorg.gradle.java.home="/home/cheeeto/wpilib/2022/jdk" $@
}

#===================================================
#
# programs to run at the start of the terminal session
#
#===================================================
#newfetch at the start of the terminal
neofetch --disable gpu
if [[ -f ~/documents/reminders.txt ]]; then
  cat ~/documents/reminders.txt
fi

#===================================================
#
# functions (environment modifiers)
#
#===================================================
#make a directory and then cd into it
mkcd ()
{
  mkdir $1
  cd $1
}
#go up n directories in the current path, if no argument is provided, just go up 1
up()
{
  numDirs=1
  if [[ ! -z "$1" ]]; then
    numDirs="$1"
  fi
  count=1;
  cdDir=".."
  while [[ $count -lt $numDirs ]]; do
    cdDir=$cdDir"/.."
    ((count++))
  done
  cd $cdDir
}
#push the previous directory in the directory stack onto the directory stack
pushpop()
{
  if [[ $(dirs | wc -w) -gt 1 ]]; then
    read -r -a array <<< $(dirs)
    dir="${array[1]}"
    dir=$(echo $dir | sed "s:~:$HOME:g") #sed accepts any character as regex delimiter
    pushd "$dir"
  else
    echo "nothing on the directory stack to push"
  fi
}

#===================================================
#
# functions (general)
#
#===================================================
ycm()
{
  #setup default values for args
  buildDir=./build

  #use passed in args
  if [[ ! -z "$1" ]]; then
    buildDir="$1"
  fi

  if [[ ! -d $buildDir ]]; then
    echo $buildDir is not a directory
    return -1
  fi

  echo searching $buildDir for 'compile_commands.json'
  pushd $buildDir > /dev/null

  #get the compile_comands file in the build directory
  compCommands=$(find -type f -name compile_commands.json)
  if [[ -z $compCommands ]]; then
    echo no compile_commands.json file found
    popd > /dev/null
    return -1
  fi

  #set the numbered vars ($1, $2, etc) to the list $compCommands
  set -- $compCommands
  fullPath=$buildDir/$1

  popd > /dev/null

  #create symlink
  fullQualPath=$(realpath $fullPath)
  echo linking to $fullQualPath
  ln -s $fullQualPath ./compile_commands.json
}

#===================================================
#
# functions (program argument modification)
#
#===================================================
vim()
{
  #create vim cache file
  export vim_cache_file=$XDG_CACHE_HOME/personal/vim_cache_file_$BASHPID
  [[ -f $vim_cache_file ]] && rm $vim_cache_file
  touch $vim_cache_file

  #if -open is provided, run the batch open version of vim
  if [[ "$1" == "-open" ]]; then
    shift

    #clear variables from any previous runs
    folder=""
    filter=""

    #check for any preset filters being used
    while [[ `echo $1 | head -c 1` == "-" ]]; do
      case $1 in
        "-cpp")
          if [[ -z $filter ]]; then
            filter="cpp|c|h|hpp|txt"
          else
            filter=$filter"|cpp|c|h|hpp|txt"
          fi
          ;;
        "-py")
          if [[ -z $filter ]]; then
            filter="py|txt"
          else
            filter=$filter"|py|txt"
          fi
          ;;
        "-java")
          if [[ -z $filter ]]; then
            filter="java|xml"
          else
            filter=$filter"|java|xml"
          fi
          ;;
        "-artist")
          if [[ -z $filter ]]; then
            filter="cpp|c|h|hpp|txt|conf|xml|py"
          else
            filter=$filter"|cpp|c|h|hpp|txt|conf|xml|py"
          fi
          ;;
        "-game")
          if [[ -z $filter ]]; then
            filter="cpp|c|h|hpp|txt|json"
          else
            filter="$filter|cpp|c|h|hpp|txt|json"
          fi
          ;;
        "-qt")
          if [[ -z $filter ]]; then
            filter="ui|qrc|pro|pri"
          else
            filter=$filter"|ui|qrc|pro|pri"
          fi
          ;;
        "-filter")
          #used for applying custom filters
          shift
          if [[ -z $filter ]]; then
            filter=$1
          else
            filter=$filter"|"$1
          fi
          ;;
        *)
          echo $1 is not a valid preset
      esac
      shift
    done

    #now that all presets are gone the next argument is the folder
    while [[ ! -z $1 ]]; do
      if [[ -z $folder ]]; then
        folder=$1
      else
        folder=$folder" "$1
      fi
      shift
    done

    #ensure a filter and folder were provided to open
    if [[ -z $folder ]]; then
      echo no folder provided
      return -1
    fi
    if [[ -z $filter ]]; then
      echo no filter provided
      return -1
    fi

    #echo provided args
    echo "filter="$filter
    echo "folder="$folder

    #cache the file and filter options used
    echo filter=$filter >> $vim_cache_file
    echo folder=$folder >> $vim_cache_file

    /usr/bin/vim `find $folder -type f | grep -iE "\.($filter)$"`
  else
    /usr/bin/vim -i NONE "$@"
  fi

  #clean environment
  rm $vim_cache_file
  unset vim_cache_file
}

