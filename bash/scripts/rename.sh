#!/bin/bash

#TODO
# 1. rewite as python script (same for all the other non OS modifying scripts)
# 2. add ability to specify which folders to run on (think the way vim -open handles folders)

useage()
{
  echo "Useage: Replaces characters in file and folder names"
  echo "  --max-depth: number of folders to recurse into, if not provided the script operates fully recursively"
  echo "  -o|--original: the characters in the file name that you want to replace"
  echo "  -n|--new: the new text that will replace the text specified with -o"
  echo "  --dirs-only: only rename directories, not files"
  echo "  --files-only: only rename files, not directories"
  echo "  -p|--print: print the results without changing file names. this may result in extra renames"
  echo "              being printed depending on the naming structure of the folders"
  exit 1
}

#cmd args
maxDepth=-1
toReplace=' '
replaceWith='_'
dirsOnly=0
filesOnly=0
printOnly=0

#read cmd args
while [[ $1 != "" ]]; do
  case $1 in
    --max-depth=[0-9]* )
      maxDepth=$(echo $1 | grep -o '[0-9]*')
      ;;
    -o | --original )
      shift
      toReplace=$1
      ;;
    -n | --new )
      shift
      replaceWith=$1
      ;;
    --dirs-only )
      dirsOnly=1
      ;;
    --files-only )
      filesOnly=1
      ;;
    -p | --print )
      printOnly=1
      ;;
    -h | --help )
      useage
      ;;
    * )
      echo "unrecognized token $1"
      useage
  esac
  shift
done

echo maxDepth=$maxDepth
echo toReplace=$toReplace
echo replaceWith=$replaceWith
echo dirsOnly=$dirsOnly
echo filesOnly=$filesOnly
echo printOnly=$printOnly
echo ""

#TODO make this a function
#calculate max depth if one isn't provided
maxBranchFound=0
maxBranch=0
while [[ $maxBranchFound -eq 0 ]]; do
  if [[ ! -z $(find -mindepth $maxBranch -maxdepth $maxBranch -type d) ]]; then
    ((maxBranch++))
  else
    maxBranchFound=1
  fi
done

#if the user provided maxdepth is lower than the calculated one, use that one instead
if [[ $maxDepth -gt -1 && $maxDepth -lt $maxBranch ]]; then
  maxBranch=$maxDepth
fi


#rename directories first
if [[ $filesOnly -ne 1 ]]; then
  echo "renaming directories first"
  currentBranch=1
  while [[ $currentBranch -le $maxBranch ]]; do
    find -maxdepth $currentBranch -mindepth $currentBranch -type d | grep -e "$toReplace" | sort | while IFS= read -r file; do
      newName=$(echo $file | sed -e "s/$toReplace/$replaceWith/g")
      #check if the new name is the same as the old name (check might be useless with the grep in teh find call)
      if [[ $file != $newName ]]; then
        #check if the new filename already exists
        if [[ -d $newName ]]; then
          echo "cant change name from $file to $newName, already exists"
          continue
        fi

        #replace
        echo "$file->$newName"
        if [[ $printOnly -ne 1 ]]; then
          mv "$file" "$newName"
        fi
      fi
    done
    ((currentBranch++))
  done
  echo ""
fi

if [[ $dirsOnly -ne 1 ]]; then
  #rename files
  echo "renaming files"
    find -maxdepth $maxBranch -type f | grep -e "$toReplace" | sort | while IFS= read -r file; do
    newName=$(echo $file | sed -e "s/$toReplace/$replaceWith/g")
    if [[ $file != $newName ]]; then
      #check if the new filename already exists
      if [[ -f $newName ]]; then
        echo "cant change name from $file to $newName, already exists"
        continue
      fi

      echo "renaming $file->$newName"
      if [[ $printOnly -ne 1 ]]; then
        mv "$file" "$newName"
      fi
    fi
  done
fi

