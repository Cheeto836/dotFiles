#!/bin/bash

#TODO figure out better way to quite all output except what i output

#useage function
useage()
{
  echo "Useage: Replaces characters in a file name"
  echo "  -a|--aurDir: the directory where aur packages to be updated are stored"
  echo "  -n|--no-clean: provide if the aur folders should be cleaned up after upgrading"
  exit 1
}

aurFolder=$HOME/aurPackages #default aur package location
clean=True #default behavior is to not clean up after installing new packages

#read cmd args
while [[ $1 != "" ]]; do
  case $1 in
    -a | --aurDir )
      shift
      aurFolder=$(realpath $1)
      ;;
    -n | --no-clean )
      clean=False
      ;;
    -h | --help )
      useage
      ;;
    * )
      echo "unrecognized token $1"
      useage
      ;;
  esac
  shift
done

#print arguments
echo "arguments:"
echo "aurFolder=$aurFolder"
echo "clean=$clean"
echo ""

#check that the aur folder exists
if [[ ! -d $aurFolder ]]; then
  echo "provided aur folder doesn't exist"
  exit 1
fi

#iterate through every aur directory
pushd $aurFolder > /dev/null
for packageDir in $(ls); do
  echo "updating $packageDir"
  pushd $packageDir > /dev/null

  #fetch updates
  git remote update
  if [[ ! -z $(git status -uno | grep "behind") ]]; then
    echo "updating from remote"
    #update and install
    git pull
    makepkg -sirc

    #check if the compressed package needs cleaned up
    if [[ $clean == "True" ]]; then
      echo "cleaning up install files"
      git clean -xfdf
    fi
  else
    echo "nothing to update... skipping"
  fi

  popd > /dev/null
  echo ""
done

