#!/bin/bash

#script vars
timeout=300000 #time to wait before turning screen off (ms) checked every 5 seconds
screenshotPath=$HOME/.cache/personal/screen.png #path to where the screenshot should be stored
lockPath=$HOME/.cache/personal/lock.png #path to where the screenshot should be stored

#initial setup
[ -f $screenshotPath ] && rm $screenshotPath
[ -f $lockPath ] && rm $lockPath

#take screenshot
scrot $screenshotPath

#blur and swirl the screenshot
convert $screenshotPath -implode 2 -spread 20 -swirl 720 $lockPath

#lock screen
i3lock -e -f -i $lockPath

while [[ $(pgrep -x i3lock) ]]; do
  [[ $timeout -lt $(xssstate -i) ]] && xset dpms force off
  sleep 5
done
