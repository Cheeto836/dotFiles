#generates a list of files to add to vim's buffers based on the filter and folder args that were
#used to open that vim.

#
#THIS SCRIPT IS ONLY INTENDED TO BE USED FROM INSIDE OF A VIM INSTANCE
#vim_cache_file environment variable only exists from inside of a vim instance
#

filter=$(cat $vim_cache_file | grep 'filter' | sed 's/.*=//g')
folder=$(cat $vim_cache_file | grep 'folder' | sed 's/.*=//g' | head -2)

find $folder -type f | grep -iE "\.($filter)$"
