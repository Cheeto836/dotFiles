#!/bin/bash

for i in $(find -type f); do
  extracted=False
  toExtract=$i
  extractLocation=$(dirname $i)
  extractLocation=$extractLocation/$(basename $i | grep -o "[^\.]*" | head -1)"/"

  echo "extracting from $toExtract to $extractLocation"

  #TODO find a better way to do this than just trying all the extract commands
  unzip $toExtract -d $extractLocation 2> /dev/null
  unrar x $toExtract $extractLocation 2> /dev/null
done
