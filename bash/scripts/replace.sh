#!/bin/bash

findRegex=""
replaceRegex=""
printOnly=0
dir="./"

help()
{
  echo "replace.sh: used to find/replace text in a group of files"
  echo "usage: replace.sh [args] <findRegex> <replaceRegex>"
  echo "Args:"
  echo "  -t|--test: don't run the command, print propsed changes"
  echo "  -d|--dir: directory to run replace on"
  echo "  -h|--help: print this message then quit"
  exit
}

handleArgs()
{
  [[ $# -lt 2 ]] && help
  while [[ ! -z "$1" ]]; do
    case "$1" in
      -t|--test)
        printOnly=1
        ;;
      -d|--dir)
        dir="$2"
        if [[ ! -d $dir ]]; then
          echo "must provide a directory after -d"
          help
        fi
        shift
        ;;
      -h|--help)
        help
        ;;
      *)
        if [[ -z $findRegex ]]; then
          findRegex="$1"
        elif [[ -z $replaceRegex ]]; then
          replaceRegex="$1"
        else
          echo "unexpected argument"
          help
        fi
    esac
    shift
  done

  if [[ -z $findRegex ]]; then
    echo "no find regex provided"
    help
  fi
  if [[ -z $replaceRegex ]]; then
    echo "no replace regex provided"
    help
  fi
}

handleArgs "$@"

echo findRegex=$findRegex
echo replaceRegex=$replaceRegex
echo printOnly=$printOnly
echo dir=$dir
echo ""

pushd $dir > /dev/null
files=$(grep -lre "$findRegex")
for file in $files; do
  echo "replacing in file $file"
  if [[ $printOnly -eq 1 ]]; then
    OIFS=$IFS
    IFS=$'\n'
    for line in $(cat $file | grep $findRegex | sed "s/^\s*//"); do
      endRes=$(echo $line | sed "s/$findRegex/$replaceRegex/g")
      echo "from: $line"
      echo "to:   $endRes"
      echo ""
    done
    IFS=$OIFS
  else
    sed -i "s/$findRegex/$replaceRegex/g" $file
  fi
  echo ""
  echo ""
  echo ""
done
popd > /dev/null

