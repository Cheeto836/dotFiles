useage()
{
  echo "Useage: calculate the max branch lenght of a directory tree"
  echo "  -d|--directory: directory to calculate the maximum branch of"
  exit 1
}

#cmd args
dir='./'

#read cmd args
while [[ $1 != "" ]]; do
  case $1 in
    -d | --directory )
      shift
      dir=$1
      ;;
    -h | --help )
      useage
      ;;
    * )
      echo "unrecognized token $1"
      useage
  esac
  shift
done

#while IFS= read -r file; do
for dir in $(find $dir -type d | sed "s/[^\/]//g"); do
  [[ $maxBranch -lt ${#dir} ]] && maxBranch=${#dir}
done

echo $maxBranch

