#!/bin/bash

if [[ $# -ne 1 ]]; then
  echo "syntax error, must provide directory to watch for downloads"
else
  dirToWatch=$1

  keepGoing=true

  while $keepGoing; do
    downloadFound=false
    for i in $(ls $dirToWatch); do
      if [[ $(echo $i | grep 'download') ]]; then
        downloadFound=true
      fi
    done
    keepGoing=$downloadFound
    sleep 2
  done
  notify-send "Download Watcher" "No more downloads in $(realpath $dirToWatch)"
fi
