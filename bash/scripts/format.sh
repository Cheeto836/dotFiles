#!/bin/bash

find -type f -name "*.h" -o -name "*.hpp" -o -name "*.cpp" -o -name "*.c" -o -name "*.java" | xargs -P 0 clang-format -i
