#!/bin/bash

if [[ ! -z $(git rev-parse --git-dir 2> /dev/null) ]]; then
  majorVersion=$(git --version | grep -o [0-9]* | head -n 1)
  minorVersion=$(git --version | grep -o [0-9]* | head -n 2 | tail -n 1)
  if [[ $majorVersion -ge 2 && $minorVersion -ge 22 ]]; then
    #doesn't handle detached head
    curBranch="$(git branch --show-current)"
    if [[ -z $curBranch ]]; then
      echo -n "$(git branch 2>/dev/null | grep '^*' | colrm 1 2)"
    else
      echo -n "($curBranch)"
    fi
  else
    #doesn't handle inited repos
    curBranch=$(git branch 2>/dev/null | grep '^*' | colrm 1 2)
    [[ -z $curBranch ]] && curBranch="master"
    echo -n $curBranch
  fi
fi

