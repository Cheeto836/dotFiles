#!/bin/bash

#prompt the user to select a vim session from all the sessions stored, if the user selects
#one then echo that session out for use in a vim function as well as add it to the vim cache file

#
#FOR USE IN VIM FUNCTIONS ONLY
#

#get the session files
files=$(ls $VIM_SESSION_FILES)
if [[ -z $files ]]; then
  exit
fi

#prompt the user to choose a session file
sessionName=$(ls $VIM_SESSION_FILES | dmenu -i -l 10 -p "Select a vim session to open: ")
echo session=$sessionName >> $vim_cache_file

echo $sessionName

