#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [[ -z $TMUX ]]; then
  if [[ -z $DISPLAY && $XDG_VTNR -eq 1 ]]; then
    exec startx
  fi
fi

#true colors with tmux
export TERM="xterm-256color"

