#!/bin/bash

while true; do
  for f in ~/pictures/backgrounds/*; do
    xwallpaper --zoom "$f"
    sleep 1h
  done
done
