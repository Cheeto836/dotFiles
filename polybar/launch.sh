#!/usr/bin/env bash

source ~/.config/polybar/scripts/envSetup.sh

# Terminate already running bar instances
killall -q polybar

# wait untill the prcesses have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

#launch bars
polybar top &
