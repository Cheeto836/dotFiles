#!/bin/bash

#check network connectivity by pinging 8.8.8.8 and return the ping time
pingTime=$(ping -c 1 8.8.8.8 | sed -n 2p | grep -o '[0-9]* ms' | grep -o '[0-9]*')

#pad the result so that it is always 4 characters long
printf -v pingTimePadded "%04d" $pingTime

echo $pingTimePadded ms
