#!/bin/bash

#home partition file system info
homeUsage=$(df -h /home | sed -n 2p | grep -o '[0-9]*%\|[0-9]*\.[0-9]*%' | grep -o '[0-9]*\|[0-9]*\.[0-9]*') 
homeFree=$(df -h /home | sed -n 2p | grep -o '[0-9A-Z]*\|[0-9A-Z]*\.[0-9A-Z]*' | tail -n2 | head -n1)

#root partition file system info
rootUsage=$(df -h / | sed -n 2p | grep -o '[0-9]*%\|[0-9]*\.[0-9]*%' | grep -o '[0-9]*\|[0-9]*\.[0-9]*') 
rootFree=$(df -h / | sed -n 2p | grep -o '[0-9A-Z]*\|[0-9A-Z]*\.[0-9A-Z]*' | tail -n2 | head -n1)

#select colors based on percent used
echo -ne "\${color white}home: "
if [[ $homeUsage -le 20 ]]; then
  echo -ne "\${color green}"
elif [[ $homeUsage -le 40 ]]; then
  echo -ne "\${color1}"
elif [[ $homeUsage -le 60 ]]; then
  echo -ne "\${color yellow}"
elif [[ $homeUsage -le 80 ]]; then
  echo -ne "\${color orange}"
else
  echo -ne "\${color red}"
fi
echo -ne "$homeFree($homeUsage%) \${color grey}| "

echo -ne "\${color white}root: "
#select colors based on percent used
if [[ $rootUsage -le 20 ]]; then
  echo -ne "\${color green}"
elif [[ $rootUsage -le 40 ]]; then
  echo -ne "\${color1}"
elif [[ $rootUsage -le 60 ]]; then
  echo -ne "\${color yellow}"
elif [[ $rootUsage -le 80 ]]; then
  echo -ne "\${color orange}"
else
  echo -ne "\${color red}"
fi
echo -ne "$rootFree($rootUsage%)"

