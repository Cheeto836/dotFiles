#!/bin/bash

#process arguments
#help()
#{
  #echo "Usage: print the programs that are using swap space"
  #echo "Arguments:"
  #echo "    -c|--cutoff: the number of entries to print"
  #echo "    -h|--help: print this message and exit"
#}

#num=-1

#while [[ ! -z "$1" ]]; do
  #echo $1
  #case $1 in
    #-c | --cutoff )
      #shift
      #num=$1
      #((num--))
      #;;
    #* )
      #help
      #exit
      #;;
  #esac
  #shift
#done

declare -a array
for process in $(find /proc -maxdepth 2 -path "/proc/[0-9]*/status" -readable); do
  #grab the information we need: the () around the awk call generate a bash array
  info=( $(awk '/VmSwap|Name|^Pid/ { print $2 }' $process 2> /dev/null) )
  #name, pid, swap
  name=${info[0]}
  pid=${info[1]}
  swap=${info[2]}

  #skip over empty entries or entries with 0 swap space usage
  [[ -z $swap ]] && continue
  [[ $swap -eq 0 ]] && continue

  array+=("$swap kb pid: $pid $name")
done

#newArray=$(echo ${array[@]} | sort)
IFS=$'\n'
newArray=($(sort -n -r <<<"${array[*]}"))

count=0
for item in ${newArray[@]}; do
  if [[ $count -gt 10 ]]; then
    exit
  fi
  echo $item
  ((count++))
done

