updateTime=$(cat /var/log/pacman.log | grep -o "^.*starting full system.*$" | grep -oE "[0-9]+-[0-9]+-[0-9]+ [0-9]+:[0-9]+" | tail -1)
date=$(echo $updateTime | grep -oE "[0-9]+-[0-9]+-[0-9]+")
time=$(echo $updateTime | grep -oE "[0-9]+:[0-9]+")

diff=$(echo "(`date +%s` - `date -d "$date $time" +%s`)" | bc)
days=$(echo "$diff / (24*3600)" | bc )
hours=$(echo "($diff % (24*3600)) / 3600" | bc)

echo -ne "Last Update: "

if [[ $days -lt 4 ]]; then
  echo -ne "%{F$(printenv blue)}"
elif [[ $days -lt 7 ]]; then
  echo -ne "%{F$(printenv green)}"
elif [[ $days -lt 11 ]]; then
  echo -ne "%{F$(printenv yellow)}"
elif [[ $days -lt 14 ]]; then
  echo -ne "%{F$(printenv orange)}"
else
  echo -ne "%{F$(printenv red)}"
fi

echo -ne $days"d "$hours"h" "%{F$(printenv foreground)}($(~/.config/bash/scripts/checkupdates | wc -l))"
