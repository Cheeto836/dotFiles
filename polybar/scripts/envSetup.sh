export background="#000000"
export foreground="#ffffff"
export red="#ff0000"
export blue="#488cf9"
export green="#00ff00"
export orange="#ef9423"
export yellow="#faff07"
export underline="#ffb52a"

#WM colors
export ws_focused_bg="#444"
export ws_empty_fg="#555"

#get wifi device name
device=$(iw dev | grep Interface | sed 's/^.*Interface //')
export wifi_interface="$device"
