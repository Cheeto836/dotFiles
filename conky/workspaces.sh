#!/bin/bash
# workspaces.sh

#bspc wm --get-status output key
#O focused and ocupied
#o unfocused and ocupied
#F focused and unocupied
#f unfocused and unocupied

# starting at 2 because the first returned is the monitor type
# however many workspaces you have; I use 10
for i in {2..11}
do

# query bspc wm for the status of the workspaces
SPACE=$(bspc wm --get-status | cut -d ":" -f $i)
	if [[ "$SPACE" == *O* || "$SPACE" == *F* ]]; then # check for focused workspace
		echo -ne "\${color orange}"
	elif [[ "$SPACE" == *o* ]]; then
		echo -ne "\${color white}" # unfocused, non-free desktop color
  elif [[ "$SPACE" == *f* ]]; then
		echo -ne "\${color grey}" # free desktop color
  else
    echo -ne "\${color blue}" # anything that isn't known, show blue
	fi
echo -ne "${SPACE:1} " # print workspaces to conky
done
