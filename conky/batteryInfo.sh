#!/bin/bash

#get battery info
batteryCharge=$(acpi | grep Battery | sed -n 1p | grep -o '[0-9]*%' | grep -o '[0-9]*')
batteryTime=$(acpi | grep Battery | sed -n 1p | grep -o '[0-9]*:[0-9]*:[0-9]*')

echo -ne "\${color white}batt"

if [[ ! $(acpi | grep Discharging) ]]; then
  echo -ne "(charging)"
fi
echo -ne ": "

#select colors based on percent used
if [[ $batteryCharge -le 20 ]]; then
  echo -ne "\${color red}"
elif [[ $batteryCharge -le 40 ]]; then
  echo -ne "\${color orange}"
elif [[ $batteryCharge -le 60 ]]; then
  echo -ne "\${color yellow}"
elif [[ $batteryCharge -le 80 ]]; then
  echo -ne "\${color1}"
else
  echo -ne "\${color green}"
fi
echo -ne "$batteryCharge% \${color white}$batteryTime "

