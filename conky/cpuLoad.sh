#!/bin/bash

getColor()
{
  if [[ $(echo "$1 <= 2.0" | bc) -eq 1 ]]; then
    echo -ne "\${color green}"
  elif [[ $(echo "$1 <= 4.0" | bc) -eq 1 ]]; then
    echo -ne "\${color1}"
  elif [[ $(echo "$1 <= 6.0" | bc) -eq 1 ]]; then
    echo -ne "\${color yellow}"
  elif [[ $(echo "$1 <= 8.0" | bc) -eq 1 ]]; then
    echo -ne "\${color orange}"
  else
    echo -ne "\${color red}"
  fi
}

uptime=$(uptime | sed -n -e 's/^.*load average: //p')
oneMin=$(echo $uptime | grep -o "[0-9]*\.[0-9]*" | head -n 1)
fiveMin=$(echo $uptime | grep -o "[0-9]*\.[0-9]*" | head -n 2 | tail -n 1)
fifteenMin=$(echo $uptime | grep -o "[0-9]*\.[0-9]*" | tail -n 1)

echo -ne "$(getColor $oneMin) $oneMin, $(getColor $fiveMin) $fiveMin, $(getColor $fifteenMin) $fifteenMin"

