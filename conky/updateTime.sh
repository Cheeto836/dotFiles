updateTime=$(cat /var/log/pacman.log | grep -o "^.*starting full system.*$" | grep -oE "[0-9]+-[0-9]+-[0-9]+ [0-9]+:[0-9]+" | tail -1)
date=$(echo $updateTime | grep -oE "[0-9]+-[0-9]+-[0-9]+")
time=$(echo $updateTime | grep -oE "[0-9]+:[0-9]+")

diff=$(echo "(`date +%s` - `date -d "$date $time" +%s`)" | bc) 
days=$(echo "$diff / (24*3600)" | bc )
hours=$(echo "($diff % (24*3600)) / 3600" | bc)

if [[ $days -lt 4 ]]; then
  echo -ne "\${color1}"
elif [[ $days -lt 7 ]]; then
  echo -ne "\${color green}"
elif [[ $days -lt 11 ]]; then
  echo -ne "\${color yellow}"
elif [[ $days -lt 14 ]]; then
  echo -ne "\${color orange}"
else
  echo -ne "\${color red}"
fi

echo -ne $days"d "$hours"h"
