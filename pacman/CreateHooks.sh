#!/bin/bash

hookDir=/usr/share/libalpm/hooks

pushd hooks > /dev/null

for file in ./*.hook; do
  fullPath=$(realpath $file)
  symlinkPath=$hookDir/$(basename $file)
  echo "creating symlink "$fullPath"->"$symlinkPath
  ln -s $fullPath $symlinkPath
done


popd > /dev/null
